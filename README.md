# Description

Script implementing captain-hook in smartbss gitlab repositories

## Prerequisites

* python3
* virtualenv package

## Setup

### Setup python dependencies

1. `cd . configuration-captain-hook-comarch-sbss`
2. (setup once) `virtualenv -p python3 captain`
3. `source capitan/bin/activate`
4. (setup once)  `pip3 install -r requirements.txt`

## Cleanup

### Delete captain virtualenv

1. `rm -r captain`

## Warnings

Before running the script, you need to generate a gitlab token and put it into the file session_gitlab.py