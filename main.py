#!/usr/bin/env python3

from os import path, mkdir
from shutil import copy
from logging import info, error, INFO, getLogger, warning
from session_gitlab import Session
from gitrepository import GitRepository
from termcolor import colored
from git import Repo, exc


class Application:
    """
    The main class
    """

    def __init__(self):
        session = Session()
        self.gl = session.get_session_gitlab()

    def __get_repository_names(self) -> list:
        return [
            project.attributes["name"] for project in self.gl.projects.list(all=True)
        ]

    def __get_repository_urls_and_names(self) -> dict:
        return {
            project.attributes["name"]: project.attributes["ssh_url_to_repo"]
            for project in self.gl.projects.list(all=True)
        }

    def __clone_repositories(self, main_directory="repo_3arts") -> None:
        repo_names_and_repo_urls = self.__get_repository_urls_and_names()
        logger = getLogger()
        logger.setLevel(INFO)
        info(colored("\nGit clone", "magenta"))
        for repository_name, ssh_url_to_repo in repo_names_and_repo_urls.items():
            try:
                full_repository_path = main_directory + "/" + repository_name
                if not path.isdir(full_repository_path):
                    Repo.clone_from(ssh_url_to_repo, full_repository_path)
                warning(f"The repository {repository_name} has already been cloned")
            except exc.GitError:
                error(
                    colored(
                        f"The cloning operation failed on the {full_repository_path} directory",
                        "red",
                    )
                )

    def __pull_repositories(self, main_directory="repo_3arts") -> None:
        name_repositories = self.__get_repository_names()
        skip_repositories = [f"{main_directory}/migration"]
        logger = getLogger()
        logger.setLevel(INFO)
        info(colored("\nGit pull", "magenta"))
        for name_repo in name_repositories:
            full_repo_path = main_directory + "/" + name_repo
            try:
                if full_repo_path not in skip_repositories:
                    info(f"Pull of the {full_repo_path} repository is in progress")
                    Repo(full_repo_path).remotes.origin.pull()
                    info(
                        colored(
                            f"Pull operation on the {full_repo_path} directory was successful",
                            "green",
                        )
                    )
            except exc.GitError:
                error(
                    colored(
                        f"Pull operation failed on the {full_repo_path} directory",
                        "red",
                    )
                )

    def __create_directory_for_repositories(self, directory: str) -> None:
        logger = getLogger()
        logger.setLevel(INFO)
        if path.isdir(directory):
            info(f"{directory} directory already exists")
        else:
            mkdir(directory)
            info(f"{directory} directory has been created")

    def __copy_captain_directory(
        self,
        main_directory="repo_3arts",
        branch_capitan="captain-hook",
        path_capitan=".captain-hook/config.json",
        commit_message="TARTS-15538 Captain Hook",
    ) -> None:
        name_repositories = self.__get_repository_names()
        for name_repo in name_repositories:
            full_repo_path = main_directory + "/" + name_repo
            path_repo_capitan = full_repo_path + "/" + ".captain-hook"
            print(colored(f"\n{full_repo_path}", "magenta"))
            captain_gitlab = GitRepository(full_repo_path)
            create_branch = captain_gitlab.create_branch(branch_capitan)
            if create_branch:
                self.__create_directory_for_repositories(path_repo_capitan)
                copy("./" + path_capitan, str("/".join([full_repo_path, path_capitan])))
                info(f"Copied directory: {path_repo_capitan}")
                captain_gitlab.commitAndPush(
                    branch_capitan, path_capitan, commit_message
                )
            else:
                warning(colored(f"Skip the {name_repo} repository", "yellow"))

    def process_scan_repositories(self, main_directory="repo_3arts"):
        """
        The function calls multiple functions
        """
        logger = getLogger()
        logger.setLevel(INFO)
        info("Start scan repositories")
        self.__create_directory_for_repositories(main_directory)
        self.__clone_repositories(main_directory)
        self.__pull_repositories(main_directory)
        self.__copy_captain_directory(main_directory)
        info("Stop scan repositories")


if __name__ == "__main__":
    app = Application()
    app.process_scan_repositories()
