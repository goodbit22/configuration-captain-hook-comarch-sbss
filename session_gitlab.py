import gitlab

class Session:
    """
    class return  gitlab endpoint
    """
    def __init__(self):
        repo_gitlab = 'http://gitlab-3arts.krakow.comarch'
        api_gitlab=repo_gitlab + "/api/v4/projects"
        self.token = ''
        self.gl = gitlab.Gitlab(repo_gitlab, private_token=self.token)
    def get_session_gitlab(self):
        """
        return session gitlab
        """
        return self.gl
