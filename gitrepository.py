from logging import INFO, getLogger, info, warning, error
from termcolor import colored
from git import Repo, exc


class GitRepository:
    """
    Class That commits ,creates branch and push changes
    """

    def __init__(self, path: str):
        self.path = path
        self.default_branch_name = "master"

    def create_branch(self, new_branch_name: str) -> bool:
        """
        The function creates capitan-hook branch
        """
        directory_repo_path = self.path
        logger = getLogger()
        logger.setLevel(INFO)
        exists = False
        info(f"A branch will be created in the {directory_repo_path} directory")
        repo = Repo(directory_repo_path)
        local_branches = [str(branch) for branch in repo.heads]
        if local_branches:
            if new_branch_name in local_branches:
                warning(f"Branch {new_branch_name} already exists")
                exists = True
            if exists:
                repo.git.checkout(new_branch_name)
            else:
                repo.git.switch("--orphan", new_branch_name)
            return True
        return False

    def commitAndPush(
        self, new_branch_name: str, file_to_commit, commit_message
    ) -> None:
        """
        The function commit and push .captain directory
        """
        logger = getLogger()
        logger.setLevel(INFO)
        repo = Repo(self.path)
        repo.index.add(file_to_commit)
        info(f"Added {file_to_commit} file".format(file_to_commit))
        repo.index.commit(commit_message)
        info(f"Commit about '{commit_message}' content was created")
        try:
            repo.git.push("--set-upstream", "origin", new_branch_name)
            info(colored(f"branch push {new_branch_name}", "green"))
        except exc.GitCommandError as err:
            error(colored(f"Skip push due to repository issue: {err}", "red"))
